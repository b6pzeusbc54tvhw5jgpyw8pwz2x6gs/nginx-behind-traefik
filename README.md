# nginx-behind-traefik

## set environment

```sh
export NGINX_HOST=my.domain.com
export NGINX_DOCKER_NETWORK_NAME=traefik_webgateway
```

## shared directory example

```sh
$ mkdir html
$ echo "<h1>hello</h1>" >> html/index.html
```

## run

```sh
$ docker-compose up -d
```

## test

```sh
$ open http://$NGINX_HOST
```